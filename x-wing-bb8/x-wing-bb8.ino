#define STEPS 200 // one revolution of a 11HS12-0674S motor (1.8° step angle, no microstepping)

#define stepPin 2 // A4988 STEP pin connected to pin 2
#define dirPin 3  // A4988 DIR pin connected to pin 3

long pos = 0; // starts at position 0 (facing forward)
long maxPosDist; // can go 55 steps in each direction to start
long maxNegDist;
long minMove = 20; // minimum amount of steps 20/200 10% or ≈36° radius
long totalAng = 110; // total of 110/200 55% or ≈200° radius
long stepsToMove;
long dir;
int speedValue;
int timeDelay;

void setup() {
  randomSeed(analogRead(0));
  pinMode(stepPin, OUTPUT);
  pinMode(dirPin, OUTPUT);
}

void loop() {
  maxNegDist = (totalAng / 2) + pos;
  maxPosDist = (totalAng / 2) - pos;

  if (maxNegDist < minMove) {
    digitalWrite(dirPin, HIGH);
    stepsToMove = random(minMove, maxPosDist);
  } else if (maxPosDist < minMove) {
    digitalWrite(dirPin, LOW);
    stepsToMove = -random(minMove, maxNegDist);
  } else {
    dir = random(2); // random direction 0 is negative, 1 is positive
    if (dir == 0) {
      digitalWrite(dirPin, LOW);
      stepsToMove = -random(minMove, maxNegDist);
    } else {
      digitalWrite(dirPin, HIGH);
      stepsToMove = random(minMove, maxPosDist);
    }
  }

  speedValue = random(400, 2001);  // random delay between steps that sets motor speed
  timeDelay = random(1000, 15001); // random delay b/t 1s-15s between movements

  for (int i = 0; i <= abs(stepsToMove); i++) {
    digitalWrite(stepPin, HIGH);
    delayMicroseconds(speedValue);
    digitalWrite(stepPin, LOW);
    delayMicroseconds(speedValue);
  }
  delay(timeDelay);
  pos = pos + stepsToMove;
}
