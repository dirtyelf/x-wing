#include <FastLED.h>

// Number of LEDs in each engine
#define NUM_LEDS_ENGINES 8

// Which pin is each group connected to
#define DATA_PIN_ENGINE1 2
#define DATA_PIN_ENGINE2 3
#define DATA_PIN_ENGINE3 4
#define DATA_PIN_ENGINE4 5

// Define the arrays of LEDs
CRGB engines[NUM_LEDS_ENGINES];

int transitionDelay = 8;
int redFlashes = 100;
int yellowFlashes = 300;
int redDelay = 3;
int yellowDelay = 3;
int warmedUpFlashes = 5000;

void setup() {
  randomSeed(analogRead(0));

  FastLED.addLeds<WS2812B, DATA_PIN_ENGINE1, RGB>(engines, NUM_LEDS_ENGINES);
  FastLED.addLeds<WS2812B, DATA_PIN_ENGINE2, RGB>(engines, NUM_LEDS_ENGINES);
  FastLED.addLeds<WS2812B, DATA_PIN_ENGINE3, RGB>(engines, NUM_LEDS_ENGINES);
  FastLED.addLeds<WS2812B, DATA_PIN_ENGINE4, RGB>(engines, NUM_LEDS_ENGINES);

  FastLED.setBrightness(255);   // brightness level 0-255

  fill_solid(engines, NUM_LEDS_ENGINES, CRGB{0, 0, 0});

  FastLED.delay(3000);  // 3s delay before startup

  // over ~5s shift from red to yellow to blue

  // start engines at red
  for (int i = 0; i < 256; i++) {
    for (int j = 0; j < NUM_LEDS_ENGINES; j++) {
      if (random(redFlashes) == 0) {
        engines[j].setRGB(255, 255, 255);
      } else {
        engines[j].setRGB(i, 0, 0);
      }
    }
    FastLED.show();
    FastLED.delay(transitionDelay);
  }

  for (int i = 0; i < 1000; i++) {
    for (int j = 0; j < NUM_LEDS_ENGINES; j++) {
      if (random(redFlashes) == 0) {
        engines[j].setRGB(255, 255, 255);
      } else {
        engines[j].setRGB(255, 0, 0);
      }
    }
    FastLED.show();
    FastLED.delay(redDelay);
  }

  // engines now yellow
  for (int i = 0; i < 256; i++) {
    for (int j = 0; j < NUM_LEDS_ENGINES; j++) {
      if (random(yellowFlashes) == 0) {
        engines[j].setRGB(255, 255, 255);
      } else {
        engines[j].setRGB(255, i, 0);
      }
    }
    FastLED.show();
    FastLED.delay(transitionDelay);
  }

  for (int i = 0; i < 1000; i++) {
    for (int j = 0; j < NUM_LEDS_ENGINES; j++) {
      if (random(yellowFlashes) == 0) {
        engines[j].setRGB(255, 255, 255);
      } else {
        engines[j].setRGB(255, 255, 0);
      }
    }
    FastLED.show();
    FastLED.delay(yellowDelay);
  }

  // engines now blue
  for (int i = 0; i < 256; i++) {
    for (int j = 0; j < NUM_LEDS_ENGINES; j++) {
      engines[j].setRGB(255 - i, 255 - i, i);
    }
    FastLED.show();
    FastLED.delay(transitionDelay);
  }
}

void loop() {
  for (int j = 0; j < NUM_LEDS_ENGINES; j++) {
    if (random(warmedUpFlashes) == 0) {
      engines[j].setRGB(255, 255, 255);
    } else {
      engines[j].setRGB(0, 0 , 255);
    }
  }
  FastLED.show();
}
